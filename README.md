# llama.cpp server

Container containing
[server](https://github.com/ggerganov/llama.cpp/tree/master/examples/server)
of [llama.cpp](https://github.com/ggerganov/llama.cpp).

## Usage

Get a gguf model from e.g. (e.g. from [🤗 Hugging
  Face](https://huggingface.co/): 🦙
  [vicuna-13b-v1.5.Q3_K_M.gguf](https://huggingface.co/TheBloke/vicuna-13B-v1.5-GGUF/blob/main/vicuna-13b-v1.5.Q3_K_M.gguf)).

In the following, it will be assumed, that this model has the filename `model.gguf`.
  
```sh
docker run -it --rm \
  -v ./model.gguf:/model.gguf \
  -p 8080:8080 \
  registry.ethz.ch/sis/container/llama.cpp-server/llama.cpp:main \
  -m /model.gguf
```

Add `--host 0.0.0.0` if you want to access the service from another
container.

When the container runs, the service can be accessed:

```sh
curl --request POST \
    --url http://localhost:8080/completion \
    --header "Content-Type: application/json" \
    --data '{"prompt": "Human: How many push ups can Chuck Norris do?\nAssistant: ","n_predict": 128}'
```

More details in the [docs](https://github.com/ggerganov/llama.cpp/tree/master/examples/server).
