FROM library/debian:stable-slim as build

RUN apt-get update && \
    apt-get install -y build-essential git

WORKDIR /
RUN git clone https://github.com/ggerganov/llama.cpp
WORKDIR /llama.cpp
RUN make server

FROM library/debian:stable-slim
COPY --from=build /llama.cpp/server /usr/local/bin/llama.cpp-server

ENTRYPOINT ["/usr/local/bin/llama.cpp-server"]
EXPOSE 8080
